﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;

namespace Paises.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        #region Atributos
        private string usuario;
        private string clave;
        private bool isEnabled;
        #endregion

        #region Propiedades
        public string Usuario
        {
            get { return usuario; }
            set
            {
                if (usuario != value)
                {
                    usuario = value;
                    OnPropertyChanged(nameof(Usuario));
                }
            }
        }
        public string Clave
        {
            get { return clave; }
            set
            {
                if (clave != value)
                {
                    clave = value;
                    OnPropertyChanged(nameof(Clave));
                }
            }
        }
        public bool IsEnabledBtn
        {
            get { return isEnabled; }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnPropertyChanged(nameof(IsEnabledBtn));
                }
            }
        }
        #endregion

        public LoginViewModel()
        {
            IsEnabledBtn = true;

            Usuario = "jperez";
            Clave = "123";
        }

        #region Atributos
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        private async void Login()
        {
            try
            {
                if (string.IsNullOrEmpty(Usuario))
                {
                    await Application.Current.
                          MainPage.DisplayAlert(
                                         "Error",
                                        "Debe Ingresar un usuario",
                                         "Aceptar");
                    return;
                }
                if (string.IsNullOrEmpty(Clave))
                {
                    await Application.Current.
                          MainPage.DisplayAlert(
                                         "Error",
                                        "Debe Ingresar la clave",
                                         "Aceptar");
                    return;
                }

                IsEnabledBtn = false;

                if (Usuario == "jperez" && Clave == "123")
                {
                    IsEnabledBtn = true;
                    await Application.Current.
                                     MainPage.Navigation.
                                     PushAsync(new PaisesPage());
                }
                else
                {
                    await Application.Current.
                          MainPage.DisplayAlert(
                                         "Alerta",
                                        "Usuario o clave Incorrecta.",
                                         "Aceptar");
                    Clave = string.Empty;
                    IsEnabledBtn = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                await Application.Current.
                          MainPage.DisplayAlert(
                                         "Error",
                                          ex.Message,
                                         "Aceptar");
                IsEnabledBtn = true;
            }
        }
        #endregion
    }
}
