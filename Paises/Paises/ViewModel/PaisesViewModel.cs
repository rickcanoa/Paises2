﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Paises.Models;

namespace Paises.ViewModel
{
    public class PaisesViewModel : BaseViewModel
    {
        //Atributos
        private string filter;
        private bool isRunning;
        private ObservableCollection<Models.Pais> paises;
        private bool isRefreshing;

        //Propiedades
        public string Filter
        {
            get { return filter; }
            set
            {

                if (filter != value)
                {
                    filter = value;
                    OnPropertyChanged(nameof(Filter));
                    Search();
                }
            }
        }
        public ObservableCollection<Models.Pais> Paises
        {
            get { return paises; }
            set
            {
                if (paises != value)
                {
                    paises = value;
                    OnPropertyChanged(nameof(Paises));
                }
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    OnPropertyChanged(nameof(IsRefreshing));
                }
            }
        }

        //Metodos
        private void Search()
        {
            throw new NotImplementedException();
        }

        public PaisesViewModel()
        {

        }

        public async Task<Response> GetList<T>(
            string urlBase,
            string servicePrefix,
            string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Ok",
                    Result = list,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
    }
}

