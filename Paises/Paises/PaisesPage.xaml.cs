﻿using Paises.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Paises
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PaisesPage : ContentPage
	{
		public PaisesPage ()
		{
            this.BindingContext =
                new PaisesViewModel();
            InitializeComponent();
        }
	}
}